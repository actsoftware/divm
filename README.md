# divm

  A Divination Machine with diagnostic tools by ACT17
  https://fearandloathing.neocities.org

# Current Information

  Name:             divm

  Version:          0.5.0
  
  Rust Version:     2021

  rustc Version:    1.76.0
  
  Release Date:     2024-02-22

# Synopsis

  *divm* is a program that generates Tarot cards.

# Building and Usage

## Packages

  Packages used:
  - ``rand``        (0.8.5)
  - ``rand_chacha`` (0.3.1)
  - ``chrono``      (0.4)

## Building

  To build, simply run ``cargo build --release`` while in the current directory. The binary will be built in ``./target/release/divm``.

  Alternatively, it can be run with ``cargo run``.

## Usage

  To use *divm*, simply follow the onscreen instructions.

# Changelog

  **0.5.0**
  - Updated ``./src/argparse.rs``
    - *Dates can now be passed into divm with the -t <ISO 8601 DATE> Command!* This has been added through a new branch in the main ``match`` statement.
    - Moved the definition of ``struct Args`` from ``main.rs`` to ``argparse.rs``.
    - Added a new field to ``struct Args``, ``time: Option<CDate>``.
  - Updated ``./src/main.rs``.
    - Added new functionality that checks if ``args.time`` is valid, and if so, instantly prints a time assessment for the passed date.
    - Finally changed the introductory line's version from ``0.3.0`` to ``0.5.0.``
  - Updated the ``TODO`` section in ``./README.md``.
    - Plans for Version 0.5.0 have been moved under the plans for Version 0.6.0.
  - Updated ``./Cargo.toml`` to reflect the newest update.

  **0.4.0**
  - Updated ``./src/assess.rs``
    - Added several new functions.
      - ``pub fn timeassess()``, which is used to provide custom dates for date assessment.
      - ``fn yearprint()``, ``fn monthprint()``, and ``fn dateprint()`` which are macros used by ``timeassess()``.
    - Updated ``dateassessment()`` to no longer take an ``&mut CDate``, but rather take a simple ``&CDate``.
  - Updated ``./src/main.rs``
    - Time assessment for the current date now prints when the program is executed.
    - The "Time Assessment" option now calls ``timeassess()`` to enter a custom date.
    - Removed unnessicary instances of mutability.

  **0.3.0**
  - Added new file: ``./src/argparse.rs``.
    - ``argparse.rs`` includes the function ``pub fn argparse(arguments: &Vec<String>) -> Args``. It parses command line arguments and returns a struct, ``Args``, meant to store various options.
  - Updated ``./src/main.rs``.
    - Added a new struct, ``pub struct Args``. It currently includes ``usehelp: usize`` which is used as a switch to see if help should be printed, ``climode: usize`` which will be used in the future, ``query: Option<String>`` which is used to store a query for a "quick Divination", ``cardnum: Option<usize>`` which is used to store the number of cards, and ``gematria: Option<usize>`` which is used to store the gematria type.
    - Added check to see if ``args.query`` is valid. If so, ``divmain::divination(&args)`` is called and the program immediately terminates thereafter.
  - Updated ``./src/divmain.rs``.
    - Updated ``pub fn divination(args: &Args)`` to now take the parameter of ``args``, and cleaned up the process to collect user information. "Steps" (E.g. the block of code that defines ``query``) are entirely skipped if they were defined by the user with command-line parameters.
  - Updated ``./src/assess/season.rs``
    - Updated ``pub fn seasoncalc()`` to now actually work properly.
    - Updated ``pub fn seasonprogcalc()`` to now have a Sine wave-like err-and-bow wherein the returned value never goes beneath 0.5.


  **0.2.1**
  - Moved ``./src/divmain/print.rs`` to ``./src/divmain/tprint.rs``.
  - Updated ``./src/divmain.rs`` to now use ``mod tprint`` instead of ``mod print``.
  - Updated ``./src/assess.rs``
    - Algorithm previously used to define ``dayinfo.dayinyear`` is now used to define a new variable; ``currentday``. Its algorithm, albiet long, is now also able to properly handle the 9/10 days after December 21st/22nd of the current calendar year which make up the first 10 days of the "new year" according to the Calendar system used by the program.
    - ``seasons``, ``moonphases``, ``monthlengths``, and ``seasonlengths`` have all been changed from Arrays into ``vec!``s.
    - Changed the algorithm for defining ``dayinfo.season`` by way of using a buffer, ``seasonvalue``, which is set to ``i``'s value until the proper season is found.
    - Changed the algorithm for defining ``dayinfo.seasonprogress`` by reversing the iterator from ``dayinfo.season..0`` to ``0..dayinfo.season``.
    - Improved accuracy of algorithm used to define ``dayinfo.moonprogress``.
    - Moved the chunks of code used to define ``dayinfo.season``, ``dayinfo.seasonprogress``, ``dayinfo.moonage``, and ``dayinfo.moonprogress`` to four new functions spread across two new files for the sake of code clarity.
    - Redundant mutability has been removed.
    - ``dayinfo`` is now defined at the end of the function and is immediately returned.
  - Added new directory ``./src/assess/``, with two new files; ``./src/assess/season.rs`` and ``./src/assess/moon.rs``.
    - ``season.rs`` contains two functions based on code originally in ``assess.rs``:
      - ``pub fn seasoncalc(dayinyear: usize, seasonlengths: &Vec<usize>) -> usize`` calculates the current season out of the year.
      - ``pub fn seasonprogcalc(dayinyear: usize, seasonlengths: &Vec<usize>, season: usize) -> f64`` calculates the progress in the current season.
    - ``moon.rs`` contains two new functions based on code originally in ``assess.rs``:
      - ``pub fn moonagecalc(date: &CDate, monthlengths: &Vec<usize>) -> f64`` calculates the Moon's age since the last New Moon in December 2023.
      - ``pub fn moonprogcalc(moonage: f64) -> f64`` calculates the moon's progress to - and from - full.

  **0.2.0** (2024-01-08)
  - Updated ``./Cargo.toml`` to add the new dependancy, ``chrono = "0.4"``, and updated the version.
  - Updated ``./src/main.rs``
    - Updated ``printmenu()`` to include a new option; *time assessment*.
    - Updated the version printed in ``main()`` to ``0.2.0``.
    - Created two new operators - ``currentdate`` and ``currentinfo`` from the new functions ``cdategrab()`` and ``dateassessment()``.
    - Added new branch in the main ``match`` statement in ``main()``. ``"1\n"`` now calls ``assessmentprint()``.
  - Added new file, ``./src/assess.rs``.
    - Contains two structs; ``pub struct CDate``. It includes three ``usize``s; ``day``, ``month``, and ``year``. Used to store information relating to a specific date. The second struct is ``pub struct DInfo``, which has two ``usize``s (``dayinyear`` and ``season``), two ``String``s (``seasonname`` and ``moonphase``), and four ``f64``s (``seasonprogress``, ``moonage``, ``moonprogress``, and ``total``).
      - ``CDate``, as stated before, stores the current date.
      - ``DInfo`` stores "esoteric" information about a date. These include:
        - ``dayinyear: usize``, a numeric count from the days since the precurring December 21st.
        - ``season: usize``, determined by if the date provided is between December 21st/22nd and February 1st, February 1st to March 20th, March 20th to April 30th, April 30th to June 20th/21st, June 20th/21st to August 1st, August 1st to September 22nd/23rd, September 22nd/23rd to October 31st, and October 31st to December 21st/22nd.
        - ``seasonname: String``, which is the name correlating to the season.
        - ``seasonprogress: f64``, the decimal value of the percent of the season which has so far transpired.
        - ``moonage: f64``, the age of the moon in the current lunar cycle.
        - ``moonphase: String``, the name given to the moon's current phase.
        - ``moonprogress: f64``, the decimal value of the percent of the moon that is illuminated.
        - ``total: f64``, the value of ``seasonprogress`` and ``moonprogress`` averaged.
    - Contains three new functions:
      - ``pub fn cdategrab() -> CDate``. Takes the current date and uses a homemade ASCII-to-Integer algorithm to stuff it into a ``CDate``.
      - ``pub fn dayassessment(date: &CDate) -> DInfo``, which extrapolates information given by ``date`` and runs various algorithms to determine the info needed to fill out a ``DInfo``, subsequently returning it.
      - ``pub fn assessmentprint(date: &CDate, info: &DInfo)``, which prints out the current date and various things from ``info``.
  - Fixed a spelling mistake in ``./README.md``.

  **0.1.1** (2023-12-24)
  - Altered ``divmain()`` in ``./src/divmain.rs``.
    - Altered the call to ``tarot::tarotprint()`` to no longer include the argument ``numberofcards``.
  - Altered ``tarotprint()`` in ``./src/divmain/tarot.rs``.
    - Altered the function to no longer include the number of cards. Now uses a more effecient iterator.
    - Each card is now printed in one line, seperated by uses of ``|`` with spaces.
    - Changed ``regcards[0]`` from ``"One"`` to ``"Ace"``.

  **0.1.0** (2023-12-24)
  Alongside Rust's default package initialization, the following has been added:
  - Added ``./README.md``.
  - Added ``./src/main.rs``. Includes the following three functions:
    - ``main()`` - Main function.
    - ``printmenu()`` - Prints the main menu.
    - ``errorprint()`` - Prints invalid input error.
  - Added ``./src/divmain.rs``. Includes the following four functions, and one struct:
    - ``divination()`` - handles Divination, including basic user input, being the main handler of data, and wraps various other functions.
    - ``printquery()`` - Notes the user to enter their query.
    - ``printnumber()`` - Prints instructions to select the number of cards.
    - ``printgematria()`` - Prints instructions to select the form of gematria.
    - ``Card { deck: usize, value: usize }`` - Stores individual card data.
  - Added ``./src/divmain/gematria.rs``. Includes the following three functions:
    - ``gematriawrapper(gytpe: u64, query: String) -> u64`` - Takes ``gtype``, and uses it to feed ``query`` as a ``Vec<char>`` to the corresponding gematria function. Returns the eventual value of ``query`` as a ``u64``, given the corresponding gematria rules. 
    - ``englishgematria(query: Vec<char>) -> u64`` - Converts ``query`` to a ``u64``, corresponding to its value in English gematria. (E.g. 'A' = 1, 'd' = 4, '!' = 0.)
    - ``asciigematria(query: Vec<char>) -> u64`` - Converts ``query`` to a ``u64``, corresponding to its value in ASCII gematria. (E.g. 'A' = 65, 'b' = 98.)
  - Added ``./src/divmain/tarot.rs``. It has the following function:
    - ``tarotgen(query: u64, cardnumber: u64) -> Vec<Card>`` - Takes ``query``, and generates random values for a singular ``Card``. Pushes randomly generated values to a ``Vec<Card>``'s individual ``Card``s, and returns.
  - Added ``./src/divmain/print.rs``. It has the following function:
    - ``tarotprint(cards: Vec<Card>,decksize: u64)`` - Prints the value of each ``Card`` converted to its corresponding value. (E.g. ``cards[i].value == 3`` prints ``Three``, ``cards[i].deck == 3`` prints ``of Swords``.)


# TODO

  **0.6.0**
  - Update DInfo
    - Add field for the progress of the year in total.
    - Add field for both the progress of the season - calculated by the day in year subtracted by the sum of all previous seasons divided by the length of the current season, *and* a "Solar Power" value which is what is currently calculated.
    - Add field for both the progress of the moon - calculated by the lunar age by the metonic cycle's length, and a "Lunar Power" value which is what is currently calculated.
  
  **0.7.0**
  - Add brief notes for each card, with functions to print such notes.

  **0.8.0**
  - Add TUI.

  **1.0.0**
  - Clean up code, ensure all edge cases are accounted for.
