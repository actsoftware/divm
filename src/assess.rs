use std::io;
use chrono::Local;

mod season;
use season::seasonprogcalc;
use season::seasoncalc;

mod moon;
use moon::moonagecalc;
use moon::moonprogcalc;

// Stores the current date.
pub struct CDate {
    pub day: usize,
    pub month: usize,
    pub year: usize,
    pub leapyear: bool,
}

// Stores various info about a given date.
pub struct DInfo {
    pub dayinyear: usize,
    pub season: usize,
    pub seasonname: String,
    pub seasonprogress: f64,

    pub moonage: f64,
    pub moonphase: String,
    pub moonprogress: f64,

    pub total: f64,
}

// Takes the given date and calculates various info about it.
pub fn dateassessment(date: &CDate) -> DInfo {

    // Seasons
    let seasons = vec!["Latter Winter","Early Spring","Latter Spring","Early Summer","Latter Summer","Early Autumn","Latter Autumn","Early Winter"];

    // Moonphases
    let moonphases = vec!["New Moon","Waxing Crescent","First Quarter","Waxing Gibbous","Full Moon","Waning Gibbous","Third Quarter","Waning Crescent"];

    // Month Lengths
    let monthlengths = {
        let mut mlengths = vec![31,28,31,30,31,30,31,31,30,31,30,31];
        if date.leapyear {
            mlengths[1] = 29;
        }
        mlengths
    };

    // Season Lengths: Distances between Dec. 21st, Feb. 1st, Mar. 20th, Apr. 30th,
    // Jun. 21st, Aug. 1st, Sep. 22nd, Oct. 31st.
    // Adjusts for potential changes.
    let seasonlengths = {
        let mut slengths = vec![42,47,41,52,41,52,39,51];
        if date.leapyear {
            slengths[1] = slengths[1] + 1;
            slengths[3] = slengths[3] - 1;
            slengths[4] = slengths[4] + 1;
        }

        if date.year % 4 > 1 {
            slengths[5] = slengths[5] + 1;
            slengths[6] = slengths[6] - 1;
        }
        if date.year - 1 % 4 == 3 {
            slengths[7] = slengths[7] + 1;
            slengths[0] = slengths[0] - 1;
        }
        slengths
    };

    // Calculating days since the beginning of the year:
    let currentday = {
        let mut buffer: usize = 0;
        for i in 0..date.month - 1 {
            buffer = buffer + monthlengths[i];
        }
        buffer = buffer + date.day;
        // Adjusting for the last 9 or 10 days of the calendar 
        // year that are the beginning of
        // the Gaelic Calendar used for this program.
        match date.month == 12 {
            true => {
                match date.year % 4 {
                    3 => {
                        if date.day < 22 {
                            buffer = buffer + 9;
                        }
                        else {
                            buffer = date.day - 21;
                        }
                    },
                    _ => {
                        if date.day < 21 {
                            buffer = buffer + 9;
                        }
                        else { 
                            buffer = date.day - 20;
                        }
                    },
                }
            }
            false => {
                match (date.year - 1) % 4 {
                    3 => {
                        buffer = buffer + 9;
                    },
                    _ => {
                        buffer = buffer + 10;
                    }
                }
            }
        }
        buffer
    };

    let dayinfo = DInfo {
        dayinyear: currentday,
        season: seasoncalc(currentday, &seasonlengths),
        seasonprogress: seasonprogcalc(currentday, &seasonlengths, seasoncalc(currentday, &seasonlengths)),
        seasonname: seasons[seasoncalc(currentday, &seasonlengths)].to_string(),
        moonage: moonagecalc(&date, &monthlengths),
        moonprogress: moonprogcalc(moonagecalc(&date, &monthlengths)),
        moonphase: moonphases[(moonagecalc(&date, &monthlengths) / 3.69132375) as usize].to_string(),
        total: (seasonprogcalc(currentday, &seasonlengths, seasoncalc(currentday, &seasonlengths)) + moonprogcalc(moonagecalc(&date, &monthlengths))) / 2.0, 
    };
    dayinfo
}

// Takes the current date and stuffs it into a cdate.
pub fn cdategrab() -> CDate {
    let current: Vec<char> = Local::now().to_string().chars().collect();
    let mut date = CDate {
        day: 0,
        month: 0,
        year: 0,
        leapyear: false,
    };
    
    date.year = {
        let mut yeartotal: usize = 0;
        for i in 0..4 {
            let fixed: i32 = (current[i] as i32 - 48) * 10_i32.pow((3 - i).try_into().unwrap());
            yeartotal = yeartotal + fixed as usize;    
        }
        yeartotal
    };

    date.month = { 
        let mut monthtotal: usize = 0;
        for i in 5..7 {
            let fixed: i32 = (current[i] as i32 - 48) * 10_i32.pow((6 - i).try_into().unwrap());
            monthtotal = monthtotal + fixed as usize;
        }
        monthtotal
    };

    date.day = {
        let mut daytotal: usize = 0;
        for i in 8..10 {
            let fixed: i32 = (current[i] as i32 - 48) * 10_i32.pow((9 - i).try_into().unwrap());
            daytotal = daytotal + fixed as usize;
        }
        daytotal
    };

    date.leapyear = {
        let mut buffer = true;
        if date.year % 100 == 0 && date.year % 400 != 0 {
            buffer = false;
        }
        else if date.year % 4 != 0 {
            buffer = false;
        }
        buffer
    };

    date
}

pub fn timeassess() {
    println!("========= CUSTOM TIME ASSESSMENT =========");

    let mut date = CDate {
        day: 0,
        month: 0,
        year: 0,
        leapyear: false,
    };

    loop {
        yearprint();
        let mut input = String::new();

        io::stdin().read_line(&mut input).expect("Failed to read input!");
        let year: usize = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
        date.year = year;
        break;
    }

    loop {
        monthprint();
        let mut input = String::new();

        io::stdin().read_line(&mut input).expect("Failed to read input!");
        let month: usize = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
        date.month = month;
        break;
    }

    loop {
        dateprint();
        let mut input = String::new();

        io::stdin().read_line(&mut input).expect("Failed to read input!");
        let day: usize = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
        date.day = day;
        break;
    }

    date.leapyear = {
        let mut buffer = true;
        if date.year % 100 == 0 && date.year % 400 != 0 {
            buffer = false;
        }
        else if date.year % 4 != 0 {
            buffer = false;
        }
        buffer
    };

    assessmentprint(&date, &dateassessment(&date));
}

fn yearprint() {
    println!("------------------------------------------");
    println!("-            ENTER THE YEAR              -");
    println!("------------------------------------------");
}

fn monthprint() {
    println!("------------------------------------------");
    println!("-            ENTER THE MONTH             -");
    println!("------------------------------------------");
}

fn dateprint() {
    println!("------------------------------------------");
    println!("-            ENTER THE DATE              -");
    println!("------------------------------------------");

}

pub fn assessmentprint(date: &CDate, info: &DInfo) {
    let months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    println!("============= TIME ASSESSMENT ============");
    println!("-          THE CURRENT DATE IS:          -");
    println!("- {} {}, {}",months[date.month - 1],date.day,date.year);
    println!("------------------------------------------");
    println!("- DAY IN YEAR:         {}",info.dayinyear);
    println!("- SEASON:              {}",info.seasonname);
    println!("- PROGRESS IN SEASON:  {}%",info.seasonprogress * 100.0);
    println!("-                                        -");
    println!("- MOON AGE:            {}",info.moonage);
    println!("- MOON PHASE:          {}",info.moonphase);
    println!("- PROGRESS OF MOON:    {}%",info.moonprogress * 100.0);
    println!("-                                        -");
    println!("- TOTAL STRENGTH:      {}%",info.total * 100.0);
    println!("------------------------------------------");
}
