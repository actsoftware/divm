use crate::assess::CDate;

// Calculates the Moon's age.
pub fn moonagecalc(date: &CDate, monthlengths: &Vec<usize>) -> f64 {
    let mut buffer: f64 = 48.53059;
    for _i in 2024..date.year {
        match date.leapyear {
            true => buffer = buffer + 366.0,
            false => buffer = buffer + 365.0,
        };
    }
    for i in 0..(date.month - 1){
        buffer = buffer + monthlengths[i] as f64;
    }
    buffer = buffer + date.day as f64;
    buffer % 29.53059
}

// Calculates the Moon's Progress
pub fn moonprogcalc(moonage: f64) -> f64 {
    match moonage <= 14.765295 {
        true => { moonage / 14.765295 }
        false => { ((29.53059 - moonage) / 29.53059) * 2.0 }
    }
}
