// Calculates the season.
pub fn seasoncalc(dayinyear: usize, seasonlengths: &Vec<usize>) -> usize {
    let mut totalsize: usize = 0;
    let mut returnvalue: usize = 0;
    for i in 0..8 {
        totalsize = totalsize + seasonlengths[i];
        if totalsize >= dayinyear {
            returnvalue = i;
            break;
        }
    }
    returnvalue
}

// Calculates the progress of the season.
pub fn seasonprogcalc(dayinyear: usize, seasonlengths: &Vec<usize>, season: usize) -> f64 {
    let mut adjusted: usize = dayinyear;
    for i in 0..season {
        adjusted = adjusted - seasonlengths[i];
    }
    if adjusted <= seasonlengths[season] / 2 {
        1.0 - (adjusted as f64 / seasonlengths[season] as f64)
    }
    else {
        adjusted as f64 / seasonlengths[season] as f64
    }
}
