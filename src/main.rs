use std::io;
use std::env;
use divmain::*;
use assess::*;
use argparse::argparse;

mod divmain;
mod assess;
mod argparse;

fn main() {
    println!("ACT's Divination Machine, Version 0.5.0");
    let args = argparse(&env::args().collect::<Vec<_>>().to_vec());

    if args.usehelp == 1 {
        arghelp();
        return;
    }

    // If a valid date is passed using command-line arguments, a Time Assessment is ran first.
    match &args.time {
        Some(date) => assessmentprint(&date,&dateassessment(&date)),
        None => (),
    };

    // If a query is passed using command-line arguments, Divination is immediately run.
    match &args.query {
        Some(_) => {
            divination(&args);
            return;
        },
        None => (),
    };

    let currentdate = cdategrab();
    let currentinfo = dateassessment(&currentdate);
    assessmentprint(&currentdate, &currentinfo);


    // Main user input.
    loop {
        printmenu();

        let mut userinput = String::new();
        io::stdin().read_line(&mut userinput).expect("Failed to read input!");

        match userinput.as_str() {

            "0\n" => {
                divination(&args);
            },

            "1\n" => {
                timeassess();
            },

            "q\n" => {
                break;
            },
            "Q\n" => {
                break;
            },

            _ => {
                errorprint(); 
            },
        };

    }
}

// Macros because it makes the code look a little prettier:
fn printmenu() {
    println!("================ MAIN MENU ===============");
    println!("------------------------------------------");
    println!("- SELECT TOOL WITH THE CORRESPONDING KEY -");
    println!("-        AND PRESS ENTER TO SELECT       -");
    println!("- 0) Divination                          -");
    println!("- 1) Time Assessment                     -");
    println!("- USE 'q' TO QUIT, OR USE CTRL+C TO KILL -");
    println!("------------------------------------------");
}

fn errorprint() {
    println!("------------------------------------------");
    println!("-         ERROR! INVALID INPUT!          -");
    println!("------------------------------------------");
}

fn arghelp() {
    println!("================ ARGS LIST ===============");
    println!("------------------------------------------");
    println!("-  ALL ARGS LISTED ARE CASE SENSITIVE    -");
    println!("- -h                Prints this message. -");
    println!("- -s/-simple        Activates 'Simple'   -");
    println!("-                   or CLI mode. For use -");
    println!("-                   where cursive is not -");
    println!("-                   compatable.          -");
    println!("- -q <QUERY>        Sets the query.      -");
    println!("- -c <# OF CARDS>   Sets the amount of   -");
    println!("-                   cards.               -");
    println!("- -g <GEMATRIA>     Sets the type of     -");
    println!("-                   Gematria used.       -");
    println!("- -t YYYY-MM-DD     Sets the date used   -");
    println!("-                   for an instant date  -");
    println!("-                   assessment.          -");
    println!("------------------------------------------");

}
