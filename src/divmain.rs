use std::io;
use tprint::*;
use gematria::*;
use crate::argparse::*;

mod tprint;
mod gematria;
mod tarot;

// Card Struct
pub struct Card {
    deck: usize,
    value: usize,
}

// Handles Divination
pub fn divination(args: &Args)  {
    println!("================ DIVINATION ===============");

    let query: String = {
        match &args.query {
            Some(x) => x.try_into().unwrap(),
            None => {
                printquery();
                let mut buffer = String::new();
                io::stdin().read_line(&mut buffer).expect("Failed to read input!");
                buffer
            },
        }
    };

    let numberofcards: u64 = {
        match &args.cardnum {
            Some(x) => (*x as usize).try_into().unwrap(),
            None => {
                printnumber();
                let mut buffer = String::new();
                io::stdin().read_line(&mut buffer).expect("Failed to read input!");
                match buffer.trim().parse() {
                   Ok(num) => num,
                   Err(_) => 3,
                }
            }
        }
    };
    
    let gematria: u64 = {
        match &args.gematria {
            Some(x) => (*x as usize).try_into().unwrap(),
            None => {
                printgematria();
                let mut buffer = String::new();
                io::stdin().read_line(&mut buffer).expect("Failed to read input!");
                match buffer.trim().parse() {
                    Ok(num) => {
                        if num < 2 {
                            num
                        }
                        else {
                            0
                        }
                    }
                    Err(_) => 0,
                }
            }
        }
    };

    let cards: Vec<Card> = tarot::tarotgen(gematriawrapper(gematria, query), numberofcards);
    tarotprint(cards);
}

pub fn printquery() {
    println!("-------------------------------------------");
    println!("-            ENTER YOUR QUERY.            -");
    println!("-------------------------------------------");
}

pub fn printnumber() {
    println!("-------------------------------------------");
    println!("-  ENTER THE AMOUNT OF CARDS YOU WISH TO  -");
    println!("-   DRAW. LEAVING BLANK WILL CAUSE THREE  -");
    println!("-               TO BE DRAWN.              -");
    println!("-------------------------------------------");
}

pub fn printgematria() {
    println!("-------------------------------------------");
    println!("-   CHOOSE FORM OF GEMATRIA TO BE USED    -");
    println!("-        AND PRESS ENTER TO SELECT        -");
    println!("- 0) English (Default)                    -");
    println!("- 1) ASCII                                -");
    println!("-   NOTE: LEAVE BLANK TO USE THE DEFAULT  -");
    println!("-------------------------------------------");
}
