use crate::assess::CDate;

pub struct Args {
    pub usehelp: usize,
    pub climode: usize,
    pub query: Option<String>,
    pub cardnum: Option<usize>,
    pub gematria: Option<usize>,
    pub time: Option<CDate>,
}

// Argument parsing.
pub fn argparse(arguments: &Vec<String>) -> Args {
    let mut args = Args {
        usehelp: 0,
        climode: 0,
        query: None,
        cardnum: None,
        gematria: None,
        time: None,
    };

    for i in 0..arguments.len() {
        match arguments[i].as_str() {

            // Help:
            "-h" => args.usehelp = 1,

            // Simple Mode:
            "-s" => args.climode = 1,
            "-simple" => args.climode = 1,
            
            // Query
            "-q" => args.query = Some(arguments[i + 1].clone()),
            
            // Card Number
            "-c" => {
                let mut buffer: usize = 0;
                let input: Vec<char> = arguments[i + 1].chars().collect();
                for i in input.len()..0 {
                    let fixed: i32 = (input[i] as i32 - 48) * 10_i32.pow((input.len() - i).try_into().unwrap());
                    buffer = buffer + fixed as usize;
                }
                match buffer > 0 {
                    true => args.cardnum = Some(buffer),
                    false => (),
                }
            },

            // Gematria Type
            "-g" => {
                let mut buffer: usize = 0;
                let input: Vec<char> = arguments[i + 1].chars().collect();
                for i in input.len()..0 {
                    let fixed: i32 = (input[i] as i32 - 48) * 10_i32.pow((input.len() - i).try_into().unwrap());
                    buffer = buffer + fixed as usize;
                }
                match buffer < 2 {
                    true => args.gematria = Some(buffer),
                    false => (),
                }
            },

            // Time Setting
            "-t" => {
                args.time = {
                    match arguments[i + 1].len() {
                        10 => {
                            let mut buffer = CDate {
                                year: 0,
                                month: 0,
                                day: 0,
                                leapyear: false,
                            };

                            let input: Vec<char> = arguments[i + 1].chars().collect();
                            let mut monthlengths = [31,28,31,30,31,30,31,31,30,31,30,31];
                            let year: String = {
                                let mut buffer: Vec<char> = vec![];
                                for i in 0..4 {
                                    buffer.push(input[i]);
                                }
                                buffer.into_iter().collect()
                            };
                            let month: String = {
                                let mut buffer: Vec<char> = vec![];
                                for i in 5..7 {
                                    buffer.push(input[i]);
                                }
                                buffer.into_iter().collect()
                            }; 
                            let day: String = {
                                let mut buffer: Vec<char> = vec![];
                                for i in 8..10 {
                                    buffer.push(input[i]);
                                }
                                buffer.into_iter().collect()
                            };
                            
                            buffer.year = match year.trim().parse() {
                                Ok(num) => num,
                                Err(_) => 0,
                            };

                            buffer.month = match month.trim().parse() {
                                Ok(num) => { if num < 13 { num } else { 0 } },
                                Err(_) => 0,
                            };

                            buffer.leapyear = {
                                let mut isleapyear = true;
                                if buffer.year % 100 == 0 && buffer.year % 400 != 0 {
                                    isleapyear = false   
                                }
                                else if buffer.year % 4 != 0 {
                                    isleapyear = false
                                }
                                isleapyear
                            };

                            match buffer.leapyear {
                                true => monthlengths[1] = 29,
                                _ => (),
                            };

                            buffer.day = match day.trim().parse() {
                                Ok(num) => {
                                    if num <= monthlengths[buffer.month] {
                                        num
                                    }
                                    else {
                                        0
                                    }
                                },
                                Err(_) => 0,
                            };
                
                            // Checking to see if it's valid
                            match buffer.year {
                                0 => None,
                                _ => match buffer.month {
                                  0 => None,
                                  _ => match buffer.day {
                                    0 => None,
                                    _ => Some(buffer)
                                  },
                                },
                            }
                        },
                        _ => None,
                    }
                };
            },

            _ => (),
        };
    }

    args
}
