use rand::prelude::*;
use rand_chacha::ChaCha8Rng;
use std::time::SystemTime;
use crate::divmain::Card;


pub fn tarotgen(query: u64, cardnumber: u64) -> Vec<Card> {

    let mut cards: Vec<Card> = Vec::new();

    for i in 0..cardnumber {
        let currentsecond = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).expect("System Time cannot be accessed!").as_secs();
        let mut rng = ChaCha8Rng::seed_from_u64(currentsecond + query - (i * 7));
        let mut currentcard = Card {
            deck: 0,
            value: 0,
        };

        currentcard.deck = rng.gen_range(0..5);
        match currentcard.deck {
            0 => {
                currentcard.value = rng.gen_range(0..22);
            },
            _ => {
                currentcard.value = rng.gen_range(0..14);
            },
        };

        cards.push(currentcard);
    }

    cards
}
