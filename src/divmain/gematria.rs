// Converts "query" into a u64, based on Gematria.
pub fn gematriawrapper(gtype: u64, query: String) -> u64 {
    
    let querychar: Vec<char> = query.chars().collect();

    match gtype {
        0 => {
            let result = englishgematria(querychar);
            result
        },

        1 => {
            let result = asciigematria(querychar);
            result
        },

        _ => {
            panic!("Something messed up! The Gematria Type is not within intended range!");
        }
    }

}

// English Ordinal.
// E.g 'A' = 1, 'a' = 1, 'M' = 13, 'm' = 13.
fn englishgematria(query: Vec<char>) -> u64 {
    
    let mut result: u64 = 0;

    for character in query {
        if character as usize > 64 && (character as usize) < 91 {
            result = result + (character as u64 - 64);
        }
        if character as usize > 96 && (character as usize) < 123 {
            result = result + (character as u64 - 96);
        }
    }

    result
}

// ASCII Ordinal
// E.g. 'A' = 65, 'b' = 98
fn asciigematria(query: Vec<char>) -> u64 {
    let mut result: u64 = 0;
    for character in query {
        result = result + (character as u64);
    }
    result
}
