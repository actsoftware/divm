use crate::divmain::Card;

pub fn tarotprint(cards: Vec<Card>) {

    let decks = ["of Pentacles","of Wands","of Cups","of Swords"];
    let regcards = ["Ace","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Page","Knight","Queen","King"];
    let arcana = ["The Fool","The Magician","The High Priestess","The Empress","The Emporer","The Hierophant","The Lovers","The Chariot","Justice","The Hermit","Wheel of Fortune","Strength","The Hanged Man","Death","Temperance","The Devil","The Tower","The Star","The Moon","The Sun","Judgement","The World"];
    
    print!("| ");

    for currentcard in cards {
        match currentcard.deck {
            0 => {
                print!("{}",arcana[currentcard.value]);
            },

            _ => {
                print!("{} {}",regcards[currentcard.value],decks[currentcard.deck - 1]);
            },
        };

        print!(" | ");
    }

    println!("");

}
